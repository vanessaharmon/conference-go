import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


# write two functions, one that makes a request to use the Pexels API,
# and one to make a request to the Open Weather API.
def get_city_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    payload = {
        "query": f"{city} {state}",
        "per_page": 1,
    }

    response = requests.get(url, params=payload, headers=headers)
    print(response)

    photo_dict = json.loads(response.content)

    try:
        return photo_dict["photos"][0]["url"]
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):
    query = f"{city},{state},US"
    api_key = OPEN_WEATHER_API_KEY
    # Create the URL for the geocoding API with the city and state
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={query}&appid={api_key}"
    # Make the request
    geocode_response = requests.get(geocode_url)
    # Parse the JSON response
    geocode_data = geocode_response.json()
    # Get the latitude and longitude from the response


    # Create the URL for the current weather API with the latitude
    #   and longitude
    lat = geocode_data[0]["lat"]
    lon = geocode_data[0]["lon"]
    # Make the request

    # Parse the JSON response

    # Get the main temperature and the weather's description and put
    #   them in a dictionary

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={api_key}"
    weather_response = requests.get(weather_url)
    weather_data = weather_response.json()

    temp = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]

    weather_dict = {
        "temp": temp,
        "description": description,
    }
    # Return the dictionary
    return weather_dict
